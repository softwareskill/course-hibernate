package pl.softwareskill.course.hibernate.crud;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować Litere C (CREATE) z CRUD dla Hibernate w trybie JPA
 * , zachowanie Hibernate oraz zawartość PersistenceContext.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPACrudCreateExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();
        tx.begin();//rozpoczęcie transakcji bo będzie modyfikacja danych

        var card = new Card();//Now encja Transient
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        //C (CREATE) z wykorzystaniem metod EntityManager
        //Możesz sprawdzić zachowanie dla różnych metod EnytityManager związanych z zapisem
        //entityManager.persist(card);
        var mergedCard = entityManager.merge(card);//Najpierw select a potem insert przy pierwszym flush

        entityManager.flush();

        var readCard = entityManager.find(Card.class, card.getCardId());

        //C (CREATE) z wykorzystaniem zapytania typu Query
        var query = entityManager.createQuery("insert into Card(cardId,cardUuid,cardOwner,enabled,cardCountry) "
                + "select :destinationId,cardUuid,cardOwner,enabled,cardCountry "
                + " from Card "
                + " where cardId=:sourceId");
        var destinationId = String.valueOf(System.currentTimeMillis());
        var sourceId = "1";
        query.setParameter("destinationId", destinationId);
        query.setParameter("sourceId", sourceId);
        var insertedCount = query.executeUpdate();
        if (insertedCount != 1) {
            throw new IllegalStateException("Kopiowanie karty nieudane");
        }

        entityManager.flush();

        tx.rollback();//Po to aby wycofać zmiany. Ale możesz zamienić na commit.

        //close resources
        entityManager.close();
    }
}
