package pl.softwareskill.course.hibernate.crud;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę D (DELETE/DESTROY) z CRUD dla Hibernate w trybie JPA
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie zmiany są rejestrowane
 * w PersistenceContext (PC)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPACrudDestroyExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        deleteTransient(entityManager);//Transakcja może być reużywana

        deleteManaged(entityManager);
        deleteDetached(entityManager);
        deleteWithQuery(entityManager);

        //close resources
        entityManager.close();
    }

    //Usunięcie encji transient
    private static void deleteTransient(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();

        var card = new Card();

        //Sprawdż zachowanie dla identyfikatora istniejącego i nieustniejącego
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid("1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        entityManager.remove(card);

        entityManager.flush();//Wymuszenie wysłania zapytań do bazy
        tx.rollback();//Celowe aby wielokrotnie wykonywać ten sam kod na istniejących danych

        //Czyścimy EM aby nie było nic w PC
        entityManager.clear();
    }

    //Usunięcie encji managed/persistent (czyli istniejącej czyli odczytanej)
    private static void deleteManaged(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);

        entityManager.remove(card);

        entityManager.flush();//Wymuszenie wysłania zapytań do bazy
        tx.rollback();//Celowe aby wielokrotnie wykonywać ten sam kod na istniejących danych

        //Czyścimy EM aby nie było nic w PC
        entityManager.clear();
    }

    //Usunięcie encji detached
    private static void deleteDetached(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);

        entityManager.detach(card);//Wypięcie z PC

        entityManager.remove(card);

        entityManager.flush();//Wymuszenie wysłania zapytań do bazy
        tx.rollback();//Celowe aby wielokrotnie wykonywać ten sam kod na istniejących danych

        //Czyścimy EM aby nie było nic w PC
        entityManager.clear();
    }

    //Usunięcie z wykorzystaniem Query
    private static void deleteWithQuery(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);

        var query = entityManager.createQuery("DELETE FROM Card where cardId=:cardId");
        query.setParameter("cardId", "1");
        int deletedCount = query.executeUpdate();//Usunięcie przze query nie powoduje aktualizacji PC

        card = entityManager.find(Card.class, "1");

        entityManager.flush();//Wymuszenie wysłania zapytań do bazy
        tx.rollback();//Celowe aby wielokrotnie wykonywać ten sam kod na istniejących danych

        //Czyścimy EM aby nie było nic w PC
        entityManager.clear();
    }

    //Usunięcie z wykorzystaniem Criteria API
    private static void deleteWithCriteria(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        var delete = criteriaBuilder.createCriteriaDelete(Card.class);

        var e = delete.from(Card.class);

        delete.where(criteriaBuilder.equal(e.get("cardId"), "1"));

        var deletedCount = entityManager.createQuery(delete).executeUpdate();//Taki delete nie aktua;lizuje PC

        card = entityManager.find(Card.class, "1");

        entityManager.flush();//Wymuszenie wysłania zapytań do bazy
        tx.rollback();//Celowe aby wielokrotnie wykonywać ten sam kod na istniejących danych

        //Czyścimy EM aby nie było nic w PC
        entityManager.clear();
    }
}
