package pl.softwareskill.course.hibernate.crud;

import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę R (READ/RETRIEVE) z CRUD dla Hibernate w trybie JPA
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie zmiany są rejestrowane
 * w PersistenceContext (PC)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPACrudReadExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        //Nie potrzeba transakcji dla odczytu w trybie JPA
        //var tx = entityManager.getTransaction();
        //tx.begin();

        var card = entityManager.find(Card.class, "1");

        var query = entityManager.createQuery("FROM Card where cardId='2'", Card.class);
        var cardFromQuery = query.getSingleResult();

        //Native query również zapewni że encja znajdzie się w PC
        var nativeQuery = entityManager.createNativeQuery("select * from CARDS where CARD_ID=:cardId", Card.class);
        nativeQuery.setParameter("cardId", "1");
        var cardFromNative = nativeQuery.getSingleResult();

        //close resources
        entityManager.close();
    }
}
