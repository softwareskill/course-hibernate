package pl.softwareskill.course.hibernate.persistencecontext;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje wysyłanie aktualizacji danych zmienionych encji do bazy danych
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityManagerExampleApplication {

    public static void main(String[] args) {
        //initialize Hibernate
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();
        tx.begin();

        Card card = createNewCard();

        entityManager.persist(card);

        entityManager.flush();

        tx.rollback();//rollback/commit zamyka sesję - trzeba nową sesję utworzyć

        var tx2 = entityManager.getTransaction();// tx == tx2
        //tx.begin();//nie rzuci wyjątku - można reużywać ale modyfikacja bez transakcji rozpoczętej rzuci wyjątek

        card = createNewCard();
        entityManager.persist(card);
        entityManager.flush();

        //close resources
        entityManager.close();
    }

    private static Card createNewCard() {
        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");
        return card;
    }

}
