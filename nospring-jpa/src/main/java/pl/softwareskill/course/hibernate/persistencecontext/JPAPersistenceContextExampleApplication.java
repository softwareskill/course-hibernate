package pl.softwareskill.course.hibernate.persistencecontext;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje proces napełniania PersistenceContext dla dwóch rodzajów encji (weryfikacja
 * w trybie debug).
 * W zależności od tego czy relacja jest LAZY czy EAGER i typu relacji w PC będą zarejestrowane różne obiekty.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPAPersistenceContextExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var cardRepository = new JPAPersistenceContextCardRepository(entityManager);
        //Możesz sprawdzić dla konfiguracji LAZY i EAGER dla relacji
        var card = cardRepository.findById("1");

        var userRepository = new JPAPersistenceContextUserRepository(entityManager);
        //Możesz sprawdzić dla konfiguracji LAZY i EAGER dla relacji
        var user = userRepository.findById("1");

        //close resources
        entityManager.close();
    }
}
