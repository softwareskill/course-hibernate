package pl.softwareskill.course.hibernate.commitrollback;

import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować działanie rollback dla Hibernate w trybie JPA. Do wykorzystania w trybie debug,
 * gdzie można zobaczyć co się dzieje w PersistenceContext
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPARollbackExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");

        tx.rollback();//Po rollback PC zostaje czyszczony
        tx.begin();

        //zaczytujemy ponownie i sprawdzamy czy będzie zapytanie
        card = entityManager.find(Card.class, "1");

        //close resources
        entityManager.close();
    }
}
