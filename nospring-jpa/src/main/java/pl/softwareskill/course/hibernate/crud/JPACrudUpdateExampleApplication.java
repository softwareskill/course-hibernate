package pl.softwareskill.course.hibernate.crud;

import java.util.UUID;
import javax.persistence.EntityManager;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę U (UPDATE) z CRUD dla Hibernate w trybie JPA
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie zmiany są rejestrowane
 * w PersistenceContext (PC)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPACrudUpdateExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        //Odkomentuj metodę, którą chcesz sprawdzić
        //updateTransient(entityManager);
        //updateManaged(entityManager);
        updateDetached(entityManager);
        //updateWithFlush(entityManager);
        //updateWithQuery(entityManager);

        //close resources
        entityManager.close();
    }

    private static void updateTransient(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();//Bez transakcji wystąpi błąd

        var card = new Card();//encja transient (bie nie odczytaliśmy jej z bazy)
        //Sprawdź dla identyfikatiora istniejącego i nieistniejącego
        card.setCardId("1");//Istniejący identyfikator
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        //Sprawdź różne metody z EntityManager dla zapisu
        entityManager.persist(card);

        //Merge tworzy nowy obiekt - zobacz referencje
        //var mergedCard = entityManager.merge(card);//Najpierw select a potem update przy pierwszym flush

        entityManager.flush();
        tx.rollback();

        entityManager.clear();
    }

    private static void updateManaged(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();//Bez transakcji wystąpi błąd

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.DE);
        card.setEnabled(!card.getEnabled());

        //Sprawdź różne metody z EntityManager dla zapisu
        var mergedCard = entityManager.merge(card);

        entityManager.flush();
        tx.commit();

        entityManager.clear();
    }

    private static void updateDetached(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();//Bez transakcji wystąpi błąd

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(!card.getEnabled());

        entityManager.detach(card);

        var mergedCard = entityManager.merge(card);
        var pcCard = entityManager.find(Card.class, card.getCardId());

        entityManager.flush();//Weryfikacja czy i jakie zapytania się wyślą
        tx.rollback();

        entityManager.clear();
    }

    private static void updateWithFlush(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();//Bez transakcji wystąpi błąd

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(!card.getEnabled());

        //Flush powoduje że zostanie wykryta zmiana i zapytanie UPDATE zostanie wysłane do bazy
        entityManager.flush();
        tx.rollback();

        entityManager.clear();
    }

    private static void updateWithQuery(EntityManager entityManager) {
        var tx = entityManager.getTransaction();
        tx.begin();//Bez transakcji wystąpi błąd

        //PObierany kartę aby sprawdzać czy aktualizacja poprze z query zaktualizuje te dane
        var card = entityManager.find(Card.class, "1");

        //Zmieniamy dane kart użytkownika ustawiając aktywnośc na przeciwną niż w odczytanej wyżej
        var query = entityManager.createQuery("UPDATE Card set enabled=:enabled where cardOwnerId=:ownerId");
        query.setParameter("ownerId", card.getCardOwnerId());
        query.setParameter("enabled", !card.getEnabled());
        int updateCount = query.executeUpdate();

        //Ponowne odpytanie
        var selectQuery = entityManager.createQuery("FROM Card where cardOwnerId=:ownerId", Card.class);
        selectQuery.setParameter("ownerId", card.getCardOwnerId());

        //Sprawdź zawartośc listy czy jest tam encja card i jakie są jej dane (czy się zaktualizowały)
        var resultList = selectQuery.getResultList();

        entityManager.refresh(card);//dopiero refresh odświeży wartość pytając bazę

        entityManager.flush();
        tx.rollback();

        entityManager.clear();
    }
}
