package pl.softwareskill.course.hibernate.persistencecontext;

import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.hibernate.User;

@RequiredArgsConstructor
class JPAPersistenceContextUserRepository {

    private final EntityManager entityManager;

    public User findById(String userId) {

        var tx = entityManager.getTransaction();
        tx.begin();

        User user = entityManager.find(User.class, userId);

        //user.getCards().clear(); //po wyczyszczeniu w PC jest informacja o bazowej zawartości

        tx.commit();//rollback i commit NIE czyści PC
        tx.begin();

        user = entityManager.find(User.class, "1"); //find po rollback/commit nie rzuci wyjątku

        return user;
    }
}