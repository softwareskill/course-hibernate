package pl.softwareskill.course.hibernate.flush;

import javax.persistence.criteria.CriteriaBuilder;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje wysyłanie aktualizacji danych zmienionych encji do bazy danych
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPAFlushExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");
        card.setCardCountry(CardCountry.PL);

        //Przed tym update zostanie wysłany Update karty powyżej jeżeli kraj karty się zmienił
        var query = entityManager.createQuery("Update Card set cardCountry='DE' where cardId='1'");
        int updatedCount = query.executeUpdate();//Po update karta nadal będzie q PL

        entityManager.refresh(card);//refresh odświeży dane

        System.out.println("Liczba zaktualizowanych wierszy dla HQL = " + updatedCount);

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        var update = criteriaBuilder.createCriteriaUpdate(Card.class);

        var e = update.from(Card.class);

        update.set("cardCountry", CardCountry.EN)
                .where(criteriaBuilder.equal(e.get("cardId"), "1"));

        //Mimo update encja nie zmieni się (query i Criteria API nie aktualizują PC)
        updatedCount = entityManager.createQuery(update).executeUpdate();
        System.out.println("Liczba zaktualizowanych wierszy dla Criteria = " + updatedCount);

        tx.rollback();//Aby pracować z istniejącymi danymi wielokrotnie

        //close resources
        entityManager.close();
    }
}
