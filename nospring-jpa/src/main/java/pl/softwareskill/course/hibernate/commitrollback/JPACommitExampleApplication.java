package pl.softwareskill.course.hibernate.commitrollback;

import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować działanie commit dla Hibernate w trybie JPA. Do wykorzystania w trybie debug,
 * gdzie można zobaczyć co się dzieje w PersistenceContext
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class JPACommitExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();
        tx.begin();

        var card = entityManager.find(Card.class, "1");

        //Zmiana danych
        card.setEnabled(!card.getEnabled());
        card.setCardCountry(CardCountry.EN);

        //Ponowne wyszukanie poprzez Query
        var cardsQuery = entityManager.createQuery("FROM  Card where id <> '1'");
        cardsQuery.setMaxResults(4); //Ograniczenie liczby wyników
        var foundCards = cardsQuery.getResultList();

        var cardToDelete = foundCards.get(0);
        entityManager.remove(cardToDelete);//Usunięcie karty

        tx.commit();
        tx.begin();

        //zaczytujemy ponownie i sprawdzamy czy będzie zapytanie o kartę po commit
        entityManager.find(Card.class, "1");

        //close resources
        entityManager.close();
    }
}
