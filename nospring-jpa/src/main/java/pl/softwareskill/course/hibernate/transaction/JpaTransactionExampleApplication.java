package pl.softwareskill.course.hibernate.transaction;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.persistencecontext.JpaHibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala sprawdzić stan transakcji, stan EntityManger dla Hibernate w trybie JPA
 * dla commit/rollback oraz to czy można tego używać ponownie po commit czy rollback.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
@Slf4j
public class JpaTransactionExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz META-INF/persistence.xml
        var entityManager = JpaHibernateInitializer.initialize();

        var tx = entityManager.getTransaction();

        log.info("1. Czy EM otwarty = {} ", entityManager.isOpen());
        log.info("1. Czy transakcja aktywna = {}", tx.isActive());
        tx.begin();

        log.info("2. Czy transakcja aktywna = {}", tx.isActive());

        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        entityManager.persist(card);

        //Sprawdź rollback
        tx.commit();
        log.info("3. Czy transakcja aktywna = {}", tx.isActive());
        log.info("3. Czy EM otwarty = {} ", entityManager.isOpen());

        //close resources
        entityManager.close();
    }
}
