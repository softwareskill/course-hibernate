# README #

Aplikacje demo SoftwareSkill dla modułu Hibernate część 1 

Moduły maven
- common-model - model danych (encje), wyjątki, konwerter wartości boolowskich. Używany w pozostałych modułach.
- database - plik SQL dla bazy danych Postgres z tabelami i danymi wykorzystywanymi w przykładach
- nospring-jpa - przykłady dla aplikacji bez Spring wykorzystujące konfigurację Hibernate jako JPA
- nospring-native - przykłady dla aplikacji bez Spring wykorzystujące natywną konfigurację Hibernate
- spring-jpa - przykłady dla aplikacji ze Spring/Spring Boot wykorzystujące Hibernate w trybie JPA
- spring-datajpa - przykłady dla aplikacji ze Spring/Spring Boot wykorzystujące Hibernate w trybie JPA oraz Spring Data JPA

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania API frameworka Hibernate.  
 
* Wersja 1.0

### Opis ###
* Funkcjonalność - aplikacje w ramach modułów realizują podobną logikę, wyszukują w bazie danych karty i użytkowników o podanym identyfikatorze i wyświetlają dane dane.
* Baza danych - baza danych zawierająca informacje o kartach i użytkownikach. Oparta o PostgreSQL
* Konfiguracja - Katalog src/main/resources. Dla Natywnego Hibernate - pliki hibernate.properties oraz hibernate.cfg.xml. Dla Hibernate w trybie JPA META-INF/persistence.xml. Dla logback plik logback.xml.Dla Spring plik application.properties  
* Zależności -  PostgreSQL, Spring Boot, Spring Data JPA, Logback (framework do logów aplikacyjnych), Hibernate
* Jak uruchomić aplikację - z linii poleceń klasy, mając w nazwie Appliaction z odpowiednimi parametrami lub z poziomu IDE. 

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka krzysztof.kadziolka@gmail.com