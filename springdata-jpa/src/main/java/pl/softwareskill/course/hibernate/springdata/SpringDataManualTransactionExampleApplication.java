package pl.softwareskill.course.hibernate.springdata;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import pl.softwareskill.course.hibernate.User;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje wykorzystanie manualnyvh transakcji w SpringData
 *
 * Uruchomienie nie wymaga podania parametrów
 */
@SpringBootApplication
@EnableTransactionManagement
@EntityScan(basePackageClasses = User.class)
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SpringDataManualTransactionExampleApplication implements CommandLineRunner {

    PlatformTransactionManager transactionManager;
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(SpringDataManualTransactionExampleApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {

        var transactionTemplate = new TransactionTemplate(transactionManager);

        //Timeout w sekundach - stworz repository dla encji UserWithLongLoad i zobacz
        //transactionTemplate.setTimeout(1);

        var result = transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                userRepository.findById("1");
            }
        });
    }
}
