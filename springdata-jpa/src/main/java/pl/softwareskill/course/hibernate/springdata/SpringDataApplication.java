package pl.softwareskill.course.hibernate.springdata;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.softwareskill.course.hibernate.User;

@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
@EnableTransactionManagement
@EntityScan(basePackageClasses = User.class)
public class SpringDataApplication implements CommandLineRunner {

    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(SpringDataApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {
        var user = userRepository.findById("1");
    }
}
