package pl.softwareskill.course.hibernate.springdata;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.User;

@Repository
interface UserRepository extends JpaRepository<User, String> {

    List<User> findByLastName(String lastName);
}