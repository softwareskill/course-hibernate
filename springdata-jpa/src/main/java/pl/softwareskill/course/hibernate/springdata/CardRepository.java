package pl.softwareskill.course.hibernate.springdata;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;

@Repository
interface CardRepository extends CrudRepository<Card, String> {

    List<Card> findAllByCardIdAndEnabledAndCardCountry(
            String cardId,
            Boolean enabled,
            CardCountry cardCountry);

    @Query("FROM Card where enabled=true and cardCountry='DE'")
    List<Card> findActiveGermanCards();

    //Przy włączonej opcji kompilatora javac z -parameters nie potrzeba @Param
    @Query("FROM Card where enabled=true and cardCountry=:country")
    List<Card> findActiveByCountry(CardCountry country);
}