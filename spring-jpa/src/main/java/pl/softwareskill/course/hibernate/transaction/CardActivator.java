package pl.softwareskill.course.hibernate.transaction;

import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.hibernate.Card;

@RequiredArgsConstructor
@Component
@Slf4j
public class CardActivator {

    private final EntityManager entityManager;

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void deactivateCard(String cardId) {
        var user = entityManager.find(Card.class, "1");
        user.setEnabled(!user.getEnabled());
    }

    @javax.transaction.Transactional
    public void deactivateCard2(String cardId) {
        var user = entityManager.find(Card.class, "1");
        user.setEnabled(!user.getEnabled());

        //Aby wycofać transakcję - rollback - należy rzucić wyjątek dziedziczący z RuntimeException
        //throw new NullPointerException("NPE");
    }
}
