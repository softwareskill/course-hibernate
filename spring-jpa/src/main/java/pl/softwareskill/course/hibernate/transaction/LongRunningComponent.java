package pl.softwareskill.course.hibernate.transaction;

import java.util.UUID;
import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.softwareskill.course.hibernate.UserWithLongLoad;

@RequiredArgsConstructor
@Component
@Slf4j
public class LongRunningComponent {

    private final EntityManager entityManager;

    @Transactional(timeout = 1)
    public void executeWithTimeout() {
        var user = entityManager.find(UserWithLongLoad.class, "1");
        log.info("Odczytano dane użytkownika");
    }

    @javax.transaction.Transactional
    public void addNewUser() {
        var newUser = new UserWithLongLoad();
        newUser.setUserId(UUID.randomUUID().toString());
        newUser.setFirstName("FirstName");
        newUser.setLastName("LastName");

        entityManager.persist(newUser);

        entityManager.flush();//Wymuszamy insert
    }
}
