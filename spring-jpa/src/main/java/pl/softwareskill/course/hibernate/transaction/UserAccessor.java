package pl.softwareskill.course.hibernate.transaction;

import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import pl.softwareskill.course.hibernate.UserWithLongLoad;

@Service
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class UserAccessor {

    PlatformTransactionManager transactionManager;
    EntityManager entityManager;

    public void loadUserWithTimeout() {

        var transactionTemplate = new TransactionTemplate(transactionManager);

        transactionTemplate.setTimeout(1);

        var result = transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                entityManager.find(UserWithLongLoad.class, "1");
            }
        });
    }
}
