package pl.softwareskill.course.hibernate.transaction;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.softwareskill.course.hibernate.User;


/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje wykorzystanie Hibernate w trybie JPA w Spring z użyciem
 * własnych implementacji Dao/Repository
 *
 * Aplikacja pokazuje działanie automatycznych transakcji w Spring, gdzie nie trzeba rozpoczynać transakcji
 * i wstawiać commit/rollback - zadziała logika Spring pod warunkiem odpowiedniej konfiguracji
 *
 * Uruchomienie nie wymaga podania parametrów
 */
@SpringBootApplication
@EnableTransactionManagement //Konfiguracja - włączenie transakcji
@EntityScan(basePackageClasses = User.class)
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SpringJpaAutoTransactionExampleApplication implements CommandLineRunner {

    CardActivator cardActivator; //Logika opakowana transakcją
    LongRunningComponent component; //Komponent dla testów timeout transakcji
    UserAccessor userAccessor; //Logika z manualnymi transkacjami w Spring

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(SpringJpaAutoTransactionExampleApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {

        //Aby transakcje działałuy automatycznie musi być wywołanie publicznej metody beana (metoda lub klasa metody
        //anotowana @Transactional)
        cardActivator.deactivateCard2("1");
    }
}
