package pl.softwareskill.course.hibernate.persistencecontext;

import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.User;

@RequiredArgsConstructor
@Repository
class HibernateWithJpaUserRepository {

    private final EntityManager entityManager;

    public User findById(String userId) {

        return entityManager.find(User.class, userId);
    }
}