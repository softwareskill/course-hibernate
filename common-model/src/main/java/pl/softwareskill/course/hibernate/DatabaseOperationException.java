package pl.softwareskill.course.hibernate;

public class DatabaseOperationException extends RuntimeException {
    public DatabaseOperationException(Throwable cause) {
        super(cause);
    }
}
