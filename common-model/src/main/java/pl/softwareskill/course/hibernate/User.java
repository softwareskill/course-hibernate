package pl.softwareskill.course.hibernate;

import java.util.List;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "USERS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString()
public class User {
    @Id
    @Column(name = "USER_ID")
    String userId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    List<CreditCard> cards;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_EMAILS", joinColumns = @JoinColumn(name = "USER_ID"))
    Set<UserEmail> emails;
}
