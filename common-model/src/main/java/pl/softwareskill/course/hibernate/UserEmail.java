package pl.softwareskill.course.hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class UserEmail {

    @Column(name = "EMAIL_ID")
    String emailId;

    @Column(name = "EMAIL_ADDRESS")
    String emailAddress;

    //Odkomentowanie i ustawienie w ramach @CollectionTable rzuci wyjątek - kolumna musi być ukryta

    //@Column(name = "USER_ID")
    //String userId;

}
