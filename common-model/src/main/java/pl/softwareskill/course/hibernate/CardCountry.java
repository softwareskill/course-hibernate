package pl.softwareskill.course.hibernate;

public enum CardCountry {
    PL,
    EN,
    DE
}
