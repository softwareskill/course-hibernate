package pl.softwareskill.course.hibernate.transaction;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala sprawdzić stan transakcji, stan EntityManger dla Hibernate w trybie natywnym
 * dla commit/rollback oraz to czy można tego używać ponownie po commit czy rollback.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
@Slf4j
public class NativeTransactionExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.getTransaction();
        log.info("1. Czy sesja otwarta = {} ", session.isOpen());
        log.info("1. Czy transakcja aktywna = {} status= {}", tx.isActive(), tx.getStatus());

        //Aby timeout zadziałał musi być długio czas operacji - można ewentualnie uzyć anotacji @PostLoad
        // i sztucznie przedłużyć czas ładowania encji - zobacz UserWithLongLoad
        //tx.setTimeout(1);//Ustawienie timeoutu w sekundach

        tx.begin();
        log.info("2. Czy transakcja aktywna = {} status= {}", tx.isActive(), tx.getStatus());

        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        session.persist(card);

        tx.commit();
        log.info("3. Czy transakcja aktywna = {} status= {}", tx.isActive(), tx.getStatus());
        log.info("3. Czy sesja otwarta = {} ", session.isOpen());
        //close resources
        session.close();
        sessionFactory.close();
    }
}
