package pl.softwareskill.course.hibernate.persistencecontext;

import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje działanie cache (czyli PersistenceContext) w Hibernate
 * i to że jeżeli dane zostaną zmodyfikowane zewnętrznie to PersistenceContext skorzysta z danych
 * w swoim cache zamiast pobrać aktualne dane z bazy danych.
 *
 * W trybie debug można dodatkjowo zobaczyć zawartość PersistenceContext.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeExternalUpdatesExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        var card = session.get(Card.class, "1");

        var query = session.createQuery("UPDATE Card set enabled=false where cardOwner=:ownerId");
        query.setParameter("ownerId", "1");
        int updateCount = query.executeUpdate();

        //W TYM MIEJSU ZATRZYMAJ APLIKACJĘ W TRYBIE DEBUG
        //Zmień dane kart w bazie danych (np. poprzez wywołanie UPDATE w PgAdmin) i przejdż dalej

        //Próba pobrania danych z bazy
        var selectQuery = session.createQuery("FROM Card where cardOwner=:ownerId", Card.class);
        selectQuery.setParameter("ownerId", "1");
        var resultList = selectQuery.getResultList();//W liście będzie obiekt z cache - dane z bazy zostaną zignorowane

        session.refresh(card);//dopiero refresh odświeży wartość pytając bazę

        //Po evict zostanie zaczytana aktualna postać danych z bazy
        session.evict(card);
        card = session.get(Card.class, "1");

        session.flush();
        tx.rollback();

        //close resources
        session.close();
        sessionFactory.close();
    }
}
