package pl.softwareskill.course.hibernate.transaction;

import pl.softwareskill.course.hibernate.HibernateInitializer;
import pl.softwareskill.course.hibernate.UserWithLongLoad;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje timeout dla transakcji dla natywnego Hibernate
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeTransactionTimeoutExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.getTransaction();
        tx.setTimeout(1);
        tx.begin();

        //Zobacz @PostLoad w klasie
        var user = session.find(UserWithLongLoad.class, "1");

        //close resources
        session.close();
        sessionFactory.close();
    }
}
