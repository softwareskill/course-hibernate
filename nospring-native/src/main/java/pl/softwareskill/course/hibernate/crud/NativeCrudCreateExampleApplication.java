package pl.softwareskill.course.hibernate.crud;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować litere C (CREATE) z CRUD dla Hibernate w trybie natywnym
 * , zachowanie Hibernate oraz zawartość PersistenceContext.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeCrudCreateExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

       //C (CREATE) z wykorzystaniem metod Session
       //Możesz sprawdzić zachowanie dla różnych metod Session związanych z zapisem
       //session.persist(card);
        //var id = session.save(card);
        //session.saveOrUpdate(card);//Najpierw select a potem insert przy pierwszym flush
        //session.update(card); //Wystąpi błąd gdyż liczba zaktualizowanych wierszy jest równa 0 bo nowy obiekt
        var mergedCard = session.merge(card);//Najpierw select a potem insert

        var pcRead = session.get(Card.class, card.getCardId());

        session.flush();

        //C (CREATE) z wykorzystaniem zapytania typu Query
        var query = session.createQuery("insert into Card(cardId,cardUuid,cardOwner,enabled,cardCountry) "
                + "select :destinationId,cardUuid,cardOwner,enabled,cardCountry "
                + " from Card "
                + " where cardId=:sourceId");
        var destinationId = String.valueOf(System.currentTimeMillis());
        var sourceId = "1";
        query.setParameter("destinationId", destinationId);
        query.setParameter("sourceId", sourceId);
        var insertedCount = query.executeUpdate();
        if (insertedCount != 1) {
            throw new IllegalStateException("Kopiowanie karty nieudane");
        }

        session.flush();

        tx.rollback();

        //close resources
        session.close();
        sessionFactory.close();
    }

}
