package pl.softwareskill.course.hibernate.crud;

import java.util.concurrent.atomic.AtomicInteger;
import org.hibernate.Session;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę U (UPDATE) z CRUD dla Hibernate w trybie natywnym
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie zmiany są rejestrowane
 * w PersistenceContext (PC)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeCrudUpdateExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        //Odkomentuj metodę, którą chcesz debugować
//        updateTransient(session);
//        updateManaged(sessionFactory.getCurrentSession());
        updateDetached(sessionFactory.getCurrentSession());
//        updateWithFlush(sessionFactory.getCurrentSession());
        updateWithQuery(sessionFactory.getCurrentSession());
//        bulkSelectWithUpdate(sessionFactory.getCurrentSession());

        //close resources
        session.close();
        sessionFactory.close();
    }

    private static void updateTransient(Session session) {
        var tx = session.beginTransaction();

        var card = new Card();
        card.setCardId("1");
        card.setCardUuid("1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        //Przetestuj różne metody związane z zapisem
        session.update(card);
        //var id = session.save(card);//Rzuci wyjatek
        //session.saveOrUpdate(card);//Najpierw select a potem update
        var mergedCard = (Card) session.merge(card);//Najpierw select a potem update

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
        //session.clear();//session clear rzuci wyjątek
    }

    private static void updateManaged(Session session) {
        var tx = session.beginTransaction();

        //Wyszukana encja - jest managed/persistent
        var card = session.find(Card.class, "1");
        card.setCardCountry(CardCountry.DE);
        card.setEnabled(!card.getEnabled());

        //Możesz sprawdzić poniższe ale nie jest to konieczne gdyż Hibernate wykryje zmiany i wywoła UPDATE
//        session.persist(card);
        //session.update(card);
//        var id = session.save(card);
//        session.saveOrUpdate(card);
//        card = (Card) session.merge(card);

        //session.flush();
        tx.rollback();
    }

    private static void updateDetached(Session session) {
        var tx = session.beginTransaction();

        var card = session.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);

        //Wypięcie - możesz sprawdzić detach/evict
        session.evict(card);

        //Odkomentuj fragment, który chcesz przetestować
//        session.persist(card);
//        var id = session.save(card);
//        session.saveOrUpdate(card);
//        session.saveOrUpdate(card);
        var mergedCard = (Card) session.merge(card);
        var pcRead = session.get(Card.class, card.getCardId());

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
    }

    private static void updateWithFlush(Session session) {
        var tx = session.beginTransaction();

        var card = session.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(!card.getEnabled());

        //Każde query powoduje iż przej jego wywołaniem niezatwierdzone zmiany zostają wysłane do bazy danych
        session.createNativeQuery("select 1").getSingleResult();

        //session.flush();
        tx.rollback();
    }

    private static void updateWithQuery(Session session) {
        var tx = session.beginTransaction();

        //Odczytanie encji
        var card = (Card) session.get(Card.class, "1");

        var query = session.createQuery("UPDATE Card set enabled=:enabled where cardOwner=:ownerId");
        query.setParameter("ownerId", card.getCardOwnerId());
        query.setParameter("enabled", !card.getEnabled());
        int updateCount = query.executeUpdate();//Wysłanie UPDATE - ale encja PC nie zostanie zaktualizowana

        //Pobieranie kart
        var selectQuery = session.createQuery("FROM Card where cardOwner=:ownerId", Card.class);
        selectQuery.setParameter("ownerId", card.getCardOwnerId());
        //W liście kart będzie card ale zmiany z UPDATE nie zostaną zaaplikowane
        var resultList = selectQuery.getResultList();

        session.refresh(card);//dopiero refresh odświeży wartość pytając bazę

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
    }

    /**
     * Pokazanie wpływu zmian na kolejne wyszukowanie
     * Pobieranie danych partiami i w ramach partii modyfikacja danych.
     * Modyfikacja danych wpływa na wynik wyszukiowania i za każdem razme wyszukiowanych wyników jest mniej
     * @param session sesja
     */
    private static void bulkSelectWithUpdate(Session session) {
        var tx = session.beginTransaction();

        var query = session.createQuery("select count(*) FROM Card where enabled=true", Long.class);
        long count = query.getSingleResult();

        System.out.println("Znaleziono " + count + " aktywnych kart");
        //pobieramy nieaktywne po 10
        long bulkCount = count / 10;
        AtomicInteger updatedCount = new AtomicInteger();
        for (long i = 0; i < bulkCount; i++) {
            //Przed każdym zapytaniem zostaną wysłane zmiany z poprzedniej iteracji i wpłyną one na wynik wyszukiwania
            var batchQuery = session.createQuery("FROM Card where enabled=true order by cardId", Card.class);
            batchQuery.setMaxResults(10);
            batchQuery.setFirstResult((int) i * 10);//Przesunięcie się w wyniku na konkretny rekord (np t3cia dziesiątka z 50)
            batchQuery.getResultList()
                    .forEach(card -> {
                        card.setEnabled(false);
                        updatedCount.getAndIncrement();
                    });
        }

        System.out.println("Zaktualizowano " + updatedCount.get() + " z " + count + " oczekiwanych");
        tx.rollback();
    }
}
