package pl.softwareskill.course.hibernate.commitrollback;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować działanie rollback dla Hibernate w trybie JPA. Do wykorzystania w trybie debug,
 * gdzie można zobaczyć co się dzieje w PersistenceContext
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeRollbackExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        var card = session.get(Card.class, "1");

        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);

        tx.rollback();//Po rollback/commit czyści się PC, nie są przywracane dane

        //sprawdzamy czy zapytanie się wykona
        card = session.get(Card.class, "1");

        //close resources
        session.close();
        sessionFactory.close();
    }

}
