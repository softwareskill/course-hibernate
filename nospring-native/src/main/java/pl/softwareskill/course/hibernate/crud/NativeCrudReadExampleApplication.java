package pl.softwareskill.course.hibernate.crud;

import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę R (READ/RETRIEVE) z CRUD dla Hibernate w trybie natywnym
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie encje są rejestrowane
 * w PersistenceContext
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeCrudReadExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        //Odkomentuj fragment, ktory będziesz chciał degug'ować
        //var card = session.get(Card.class, "1");
//        card = session.find(Card.class, "1");
//
//        var card2 = session.load(Card.class, "2");
//
//        card = (Card) session.load("Card", "1");
//
//        var query = session.createQuery("FROM Card where cardId='2'", Card.class);
//        var cardFromQuery = query.getSingleResult();
//
//        var partialQuery = session.createQuery("SELECT cardId,cardCountry FROM Card where cardId='2'", Card.class);
//        var partialCardFromQuery = partialQuery.getSingleResult();

//        var nativeQuery = session.createNativeQuery("select * from CARDS where CARD_ID=:cardId", Card.class);
//        nativeQuery.setParameter("cardId", "1");
//        var cardFromNative = nativeQuery.getSingleResult();

        var optionalCard = session.byId(Card.class).loadOptional("1");

        //Jeżeli encja jest w PersistenceContext to częściowe zapytanie się powiedzie dlatego że HB weźmie dla idka obiekt z PC
        //Jeśli encji nie będzie to wystąpi błąd, bo muszą być wszystkie property encji zmapowane
        var nativePartialQuery = session.createNativeQuery("select CARD_ID,COUNTRY from CARDS where CARD_ID=:cardId", Card.class);
        nativePartialQuery.setParameter("cardId", "1");
        var cardFromNativePartial = nativePartialQuery.getSingleResult();

        tx.rollback();

        //close resources
        session.close();
        sessionFactory.close();
    }

}
