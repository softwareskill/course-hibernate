package pl.softwareskill.course.hibernate.crud;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.hibernate.CardCountry;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartialCard {
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;
}
