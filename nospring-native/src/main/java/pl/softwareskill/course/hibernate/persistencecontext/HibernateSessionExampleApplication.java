package pl.softwareskill.course.hibernate.persistencecontext;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje korzystanie z sesji i transakcji oraz co się stanie
 * po rollback lub commit.
 *
 * W trybie debug można dodatkjowo zobaczyć zawartość PersistenceContext.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateSessionExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        Card card = createNewCard();

        session.persist(card);

        session.flush();

        tx.rollback();//rollback/commit zamyka sesję - trzeba nową sesję utworzyć

        var card2 = session.get(Card.class, "1");//Po rollback/commit bez nowej sesji i transakcji rzucony zostanie wyjątek

        tx.begin();//rzuci wyjątek

        //close resources
        session.close();
        sessionFactory.close();
    }

    private static Card createNewCard() {
        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");
        return card;
    }

}
