package pl.softwareskill.course.hibernate.flush;

import java.util.UUID;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje kiedy zadziała automatyczny flush w Hibernate
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeFlushExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        var card = session.get(Card.class, "1");

        card.setCardUuid(UUID.randomUUID().toString());//Zmiana danych

        //Nie wykona się flush - musi być zapytanie typu query
        card = session.get(Card.class, "2");

        //Wykona się flush bo jest query
        var query = session.createQuery("FROM Card where cardId='2'", Card.class);
        query.getSingleResult();

        tx.commit();

        //close resources
        session.close();
        sessionFactory.close();
    }

}
