package pl.softwareskill.course.hibernate.crud;

import org.hibernate.Session;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.CardCountry;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pozwala zaprezentować literę D (DELETE/DESTROY) z CRUD dla Hibernate w trybie natywnym
 * , zachowanie Hibernate oraz zawartość PersistenceContext. W trybie debug można zobaczyć jakie zmiany są rejestrowane
 * w PersistenceContext (PC)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeCrudDestroyExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        //Odkomentuj metodę, którą chcesz użyć
//        deleteTransient(session);
//        deleteManaged(sessionFactory.getCurrentSession());
//        deleteDetached(sessionFactory.getCurrentSession());
        deleteWithQuery(sessionFactory.getCurrentSession());

        //close resources
        session.close();
        sessionFactory.close();
    }

    private static void deleteTransient(Session session) {
        var tx = session.beginTransaction();//Bez transakcji nie zadziała nawet odczyt dla natywnego trybu

        var card = new Card();//encja transient
        //Sprawdź z nieistniejącym rekordem
        card.setCardId("1");//istniejący identyfikator
        card.setCardUuid("1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(false);
        card.setCardOwnerId("1");

        //Sprawdź remove/delete
        session.remove(card);
        //session.delete(card);

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
        //session.clear();//session clear rzuci wyjątek
    }

    private static void deleteManaged(Session session) {
        var tx = session.beginTransaction();//Bez transakcji nie zadziała nawet odczyt dla natywnego trybu

        var card = session.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);
        card.setEnabled(!card.getEnabled());

        //Sprawdź remove/delete
        session.remove(card);
        //session.delete(card);

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
    }

    private static void deleteDetached(Session session) {
        var tx = session.beginTransaction();//Bez transakcji nie zadziała nawet odczyt dla natywnego trybu

        var card = session.find(Card.class, "1");
        card.setCardCountry(CardCountry.EN);

        session.evict(card);

        session.remove(card);
        //session.delete(card);

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
    }

    private static void deleteWithQuery(Session session) {
        var tx = session.beginTransaction();//Bez transakcji nie zadziała nawet odczyt dla natywnego trybu

        var card = (Card) session.get(Card.class, "1");

        //USunięcie poprzez query nie aktualizuje danych w PersistenceContext
        var query = session.createQuery("DELETE FROM Card cardId=:cardId");
        query.setParameter("cardId", "1");
        int deletedCount = query.executeUpdate();

        card = session.get(Card.class, "1");

        session.flush();//Wysłanie zmian zarejestrowanych w Hibernate do bazy danych
        tx.rollback();//Planowane - aby wielokrotnie korzystać z tych samych danych
    }
}
